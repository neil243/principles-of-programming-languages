//***************************
/*
    This class provides a recursive descent parser 
    for Corgi (the new version),
    creating a parse tree which can be interpreted
    to simulate execution of a Corgi program
*/

import java.util.*;
import java.io.*;

public class Parser2 {

   private Lexer lex;

   public Parser2( Lexer lexer ) {
      lex = lexer;
   }

   private Node parseExpr() {
      System.out.println("-----> parsing <expr>");
      Token token = lex.getNextToken();

      if(token.isKind("NUMBER"))
      {
         return new Node("NUMBER", token.getDetails(), null, null, null );
      }
      
      else if(token.isKind("NAME"))
      {        
        return new Node("NAME", token.getDetails(), null, null, null);
      }

      else if(token.matches("SINGLE", "("))
      {
        lex.putBackToken( token );
        Node first = parseList();
        return first;
      }

      else // error
      {
         System.out.println("expected NUMBER, NAME, or ( and saw " + token );
         System.exit(1);
         return null;
      }

   }// <expr>

   public Node parseList() {
      System.out.println("-----> parsing <list>:");

      Token token = lex.getNextToken();
      errorCheck(token, "SINGLE", "(");

      token = lex.getNextToken();

      if(token.matches("SINGLE", ")"))
      {
        return new Node("list", "empty", null, null, null);
      }

      else if (token.isKind("NAME"))
      {
        if ( token.getDetails().equals("if") )
        {
          Node first = parseExpr();
          Node second = parseExpr();
          Node third = parseExpr();
          token = lex.getNextToken();
          errorCheck(token, "SINGLE", ")");
          return new Node("if", first, second, third);
        }
        else if ( token.getDetails().equals("plus") || token.getDetails().equals("minus") ||
                  token.getDetails().equals("times") || token.getDetails().equals("div") ||
                  token.getDetails().equals("lt") || token.getDetails().equals("le") ||
                  token.getDetails().equals("eq") || token.getDetails().equals("ne") ||
                  token.getDetails().equals("and") || token.getDetails().equals("or") ||
                  token.getDetails().equals("ins"))
        {
          Node first = parseExpr();
          Node second = parseExpr();
          Token name = token;
          token = lex.getNextToken();
          errorCheck(token, "SINGLE", ")");
          return new Node("funcCall", name.getDetails(), first, second, null);
        }
        else if ( token.getDetails().equals("not") || token.getDetails().equals("first") ||
                  token.getDetails().equals("rest") || token.getDetails().equals("null") ||
                  token.getDetails().equals("num") || token.getDetails().equals("list") ||
                  token.getDetails().equals("write") || token.getDetails().equals("quote"))
        {
          Node first = parseExpr();
          Token name = token;
          token = lex.getNextToken();
          errorCheck(token, "SINGLE", ")");
          return new Node("funcCall", name.getDetails(), first, null, null);
        }
        else if ( token.getDetails().equals("read") || token.getDetails().equals("nl") ||
                  token.getDetails().equals("quit"))
        {
          return new Node("funcCall", token.getDetails(), null, null, null);
        }

        else // For user defined functions
        {
          Token name = token;
          
          token = lex.getNextToken();
          if (token.matches("SINGLE", ")"))
          {
            return new Node("funcCall", name.getDetails(), null, null, null);
          }
          
          lex.putBackToken( token );
          Node first = parseItems();

          token = lex.getNextToken();
          errorCheck(token, "SINGLE", ")");
          return new Node("funcCall", name.getDetails(), first, null, null);
        }
      }

      else if (token.isKind("NUMBER") || token.matches("SINGLE", "("))
      {
        lex.putBackToken( token );
        Node first = parseItems();

        token = lex.getNextToken();
        errorCheck(token, "SINGLE", ")");

        return new Node("list", "", first, null, null);
      }

      else
      {
        System.out.println("Error: cannot have token: " + token);
        System.exit(1);
        return null;
      }

   }// <list>

  public Node parseItems()
  {
    System.out.println("-----> parsing <args>:");

    Node first = parseExpr();

    Token token = lex.getNextToken();
    if (token.matches("SINGLE", ")"))
    {
      lex.putBackToken( token );
      return new Node ("items", "", first, null, null);
    }

    lex.putBackToken( token );
    Node second = parseItems();
    
    return new Node("items", "", first, second, null);

  } //<args>

  // check whether token is correct kind
  private void errorCheck( Token token, String kind ) {
    if( ! token.isKind( kind ) ) {
      System.out.println("Error:  expected " + token + 
                         " to be of kind " + kind );
      System.exit(1);
    }
  }

  // check whether token is correct kind and details
  private void errorCheck( Token token, String kind, String details ) {
    if( ! token.isKind( kind ) || 
        ! token.getDetails().equals( details ) ) {
      System.out.println("Error:  expected " + token + 
                          " to be kind= " + kind + 
                          " and details= " + details );
      System.exit(1);
    }
  }

  private void errorCheckReservedWord(Token token)
  {
    if ( token.getDetails().equals("define") || token.getDetails().equals("plus") ||
                 token.getDetails().equals("minus") || token.getDetails().equals("times") ||
                 token.getDetails().equals("div") || token.getDetails().equals("lt") ||
                 token.getDetails().equals("le") || token.getDetails().equals("eq") ||
                 token.getDetails().equals("ne") || token.getDetails().equals("and") ||
                 token.getDetails().equals("or") || token.getDetails().equals("not") ||
                 token.getDetails().equals("ins") || token.getDetails().equals("first") ||
                 token.getDetails().equals("rest") || token.getDetails().equals("null") ||
                 token.getDetails().equals("num") || token.getDetails().equals("list") ||
                 token.getDetails().equals("read") || token.getDetails().equals("write") ||
                 token.getDetails().equals("nl") || token.getDetails().equals("quote") ||
                 token.getDetails().equals("quit") || token.getDetails().equals("if") )
    {
      System.out.println("Error: cannot redefine " + token.getDetails() + 
                          "\nToken: " + token.getDetails() + " is a reserved word.");
      System.exit(1);
    }
  }

}