/*  a Node holds one node of a parse tree
    with several pointers to children used
    depending on the kind of node
*/

    import java.util.*;
    import java.io.*;
    import java.awt.*;

    public class Node {

  public static int count = 0;  // maintain unique id for each node

  private int id;

  private String kind;  // non-terminal or terminal category for the node
  private String info;  // extra information about the node such as
                        // the actual identifier for an I

  // references to children in the parse tree
  private Node first, second, third; 

  // stack of memories for all pending calls
  private static ArrayList<MemTable> memStack = new ArrayList<MemTable>();
  // convenience reference to top MemTable on stack
  private static MemTable table = new MemTable();

  private static Node root;  // root of the entire parse tree

  private static Scanner keys = new Scanner( System.in );

  // construct a common node with no info specified
  public Node( String k, Node one, Node two, Node three ) {
    kind = k;  info = "";  
    first = one;  second = two;  third = three;
    id = count;
    count++;
    System.out.println( this );
  }

  // construct a node with specified info
  public Node( String k, String inf, Node one, Node two, Node three ) {
    kind = k;  info = inf;  
    first = one;  second = two;  third = three;
    id = count;
    count++;
    System.out.println( this );
  }

  // construct a node that is essentially a token
  public Node( Token token ) {
    kind = token.getKind();  info = token.getDetails();  
    first = null;  second = null;  third = null;
    id = count;
    count++;
    System.out.println( this );
  }

  public String toString() {
    return "#" + id + "[" + kind + "," + info + "]<" + nice(first) + 
    " " + nice(second) + ">";
  }

  public String nice( Node node ) {
   if ( node == null ) {
    return "-";
  }
  else {
    return "" + node.id;
  }
}

  // produce array with the non-null children
  // in order
private Node[] getChildren() {
  int count = 0;
  if( first != null ) count++;
  if( second != null ) count++;
  if( third != null ) count++;
  Node[] children = new Node[count];
  int k=0;
  if( first != null ) {  children[k] = first; k++; }
  if( second != null ) {  children[k] = second; k++; }
  if( third != null ) {  children[k] = third; k++; }

  return children;
}

  //******************************************************
  // graphical display of this node and its subtree
  // in given camera, with specified location (x,y) of this
  // node, and specified distances horizontally and vertically
  // to children
public void draw( Camera cam, double x, double y, double h, double v ) {

  System.out.println("draw node " + id );

    // set drawing color
  cam.setColor( Color.black );

  String text = kind;
  if( ! info.equals("") ) text += "(" + info + ")";
  cam.drawHorizCenteredText( text, x, y );

    // positioning of children depends on how many
    // in a nice, uniform manner
  Node[] children = getChildren();
  int number = children.length;
  System.out.println("has " + number + " children");

  double top = y - 0.75*v;

  if( number == 0 ) {
    return;
  }
  else if( number == 1 ) {
    children[0].draw( cam, x, y-v, h/2, v );     cam.drawLine( x, y, x, top );
  }
  else if( number == 2 ) {
    children[0].draw( cam, x-h/2, y-v, h/2, v );     cam.drawLine( x, y, x-h/2, top );
    children[1].draw( cam, x+h/2, y-v, h/2, v );     cam.drawLine( x, y, x+h/2, top );
  }
  else if( number == 3 ) {
    children[0].draw( cam, x-h, y-v, h/2, v );     cam.drawLine( x, y, x-h, top );
    children[1].draw( cam, x, y-v, h/2, v );     cam.drawLine( x, y, x, top );
    children[2].draw( cam, x+h, y-v, h/2, v );     cam.drawLine( x, y, x+h, top );
  }
  else {
    System.out.println("no Node kind has more than 3 children???");
    System.exit(1);
  }

  }// draw

  public static void error( String message ) {
    System.out.println( message );
    System.exit(1);
  }

  // ===============================================================
  // evaluate nodes
  // ===============================================================
  
  public Value evaluate(Node rootFuncDefs) 
  {
    if ( kind.equals("NAME") ) 
    {
      return new Value(table.retrieve( info ));
    }// NAME

    else if ( kind.equals("NUMBER") ) 
    {
      return new Value(Double.parseDouble( info ));
    }// NUMBER

    else if (kind.equals("list"))
    {
      Value listOfValues = new Value(); // Create a linked list of values
      ArrayList<Value> list = new ArrayList<Value>();

      Node node = first;

      while(node != null)
      {
        list.add(node.first.evaluate(rootFuncDefs));
        node = node.second;
      }

      // Here we start with the last element because we can only add elements
      // to a list value starting at the front. So we start with the last element
      // and build our way to the first.
      for(int i = list.size() - 1; i >= 0; i--)
      {
        listOfValues = listOfValues.insert(list.get(i));
      }

      return listOfValues;

    }

    else if (kind.equals("if"))
    { 
      Value value = first.evaluate(rootFuncDefs);

      if(!value.numericEquals(Value.ZERO))
      {
        value = second.evaluate(rootFuncDefs);
      }

      else
      {
        value = third.evaluate(rootFuncDefs);
      }

      return value;
    }

    else if(kind.equals("items"))
    {
      return first.evaluate(rootFuncDefs);
    }

    else if ( kind.equals("funcCall") ) 
    {
      // execute a function call to produce a value
      String funcName = info;
      Value value = new Value();  // have all function calls put their value here
                                  // to return once at the bottom

      // handle bifs
      if ( member( funcName, bif0 ) ) 
      {
        if ( funcName.equals("read") )
        {
          System.out.print(">");
          value =  new Value( keys.nextDouble() );
        }
        else if (funcName.equals("nl"))
        {
          System.out.println();
        }
        else if (funcName.equals("quit"))
        {
          System.out.print(".......execution halting");
          System.exit(0);
        }

      }// bif0

      else if ( member( funcName, bif1 ) ) 
      {
        if ( funcName.equals("not") )
        {
          Value arg1 = first.evaluate(rootFuncDefs);
          value = arg1.getNumber() == 0 ? Value.ONE : Value.ZERO;
        }

        else if ( funcName.equals("first") )
        {
          if(first.info.equals("empty"))
          {
            System.out.print("Error: cannot return first from an empty list");
            System.exit(1);
          }
          
          value = first.evaluate(rootFuncDefs);
          value = value.first();
        }
        
        else if ( funcName.equals("rest") ) 
        {
          value = first.evaluate(rootFuncDefs);
          value = value.rest();
        }

        else if ( funcName.equals("null") )
        {
          value = first.evaluate(rootFuncDefs);
          value = value.isEmpty() ? Value.ONE : Value.ZERO;
        }

        else if ( funcName.equals("num") ) 
        {
          value = first.evaluate(rootFuncDefs);
          value = value.isNumber() ? Value.ONE : Value.ZERO;
        }

        else if ( funcName.equals("list") ) 
        {
          value = first.evaluate(rootFuncDefs);
          value = !value.isNumber() ? Value.ONE : Value.ZERO;
        }

        else if ( funcName.equals("write") ) 
        {
          if(first.info.equals("empty")) // case (write ())
          {
            value = new Value();
          }
          else
          {
            value = first.evaluate(rootFuncDefs);
          }
        }

        else if ( funcName.equals("quote") ) // Need clarification on how quote is supposed to work
        {
          value = first.evaluate(rootFuncDefs);
          return value;
        }
      }

      else if ( member( funcName, bif2 ) ) 
      {
        if ( funcName.equals("plus") ) 
        {
          Value value1 = first.evaluate(rootFuncDefs);
          Value value2 = second.evaluate(rootFuncDefs);
          value  = new Value( value1.getNumber() + value2.getNumber() );
          return value;
        }

        else if ( funcName.equals("minus") ) 
        {
          Value value1 = first.evaluate(rootFuncDefs);
          Value value2 = second.evaluate(rootFuncDefs);
          value  = new Value( value1.getNumber() - value2.getNumber() );
          return value;
        }

        else if ( funcName.equals("times") ) 
        {
          Value value1 = first.evaluate(rootFuncDefs);
          Value value2 = second.evaluate(rootFuncDefs);
          value  = new Value( value1.getNumber() * value2.getNumber() );
          return value;
        }

        else if ( funcName.equals("div") ) 
        {
          Value value1 = first.evaluate(rootFuncDefs);
          Value value2 = second.evaluate(rootFuncDefs);
          value  = new Value( value1.getNumber() / value2.getNumber() );
          return value;
        }

        else if ( funcName.equals("lt") ) 
        {
          Value value1 = first.evaluate(rootFuncDefs);
          Value value2 = second.evaluate(rootFuncDefs);

          value = value1.getNumber() < value2.getNumber() ? Value.ONE : Value.ZERO;
        }

        else if ( funcName.equals("le") ) 
        {
          Value value1 = first.evaluate(rootFuncDefs);
          Value value2 = second.evaluate(rootFuncDefs);

          value = value1.getNumber() <= value2.getNumber() ? Value.ONE : Value.ZERO;
        }

        else if ( funcName.equals("eq") ) 
        {
          Value value1 = first.evaluate(rootFuncDefs);
          Value value2 = second.evaluate(rootFuncDefs);

          value = value1.getNumber() == value2.getNumber() ? Value.ONE : Value.ZERO;
        }

        else if ( funcName.equals("ne") ) 
        {
          Value value1 = first.evaluate(rootFuncDefs);
          Value value2 = second.evaluate(rootFuncDefs);

          value = value1.getNumber() != value2.getNumber() ? Value.ONE : Value.ZERO;
        }

        else if ( funcName.equals("and") ) 
        {
          Value value1 = first.evaluate(rootFuncDefs);
          Value value2 = second.evaluate(rootFuncDefs);

          value = value1.getNumber()!=0 && value2.getNumber()!=0 ? Value.ONE : Value.ZERO;
        }

        else if ( funcName.equals("or") ) 
        {
          Value value1 = first.evaluate(rootFuncDefs);
          Value value2 = second.evaluate(rootFuncDefs);

          value = value1.getNumber()!=0 || value2.getNumber()!=0 ? Value.ONE : Value.ZERO;
        }

        else if ( funcName.equals("ins") ) 
        {
          Value arg1 = first.evaluate(rootFuncDefs);
          value = second.evaluate(rootFuncDefs);
          value = value.insert(arg1);
        }
        
        else 
        {
          error("unknown bif2 name [" + funcName + "]");
          value = new Value( -1 );
        }
      }

      else // user-defined function
      {
        Node body = passArgs( this, rootFuncDefs, funcName );
        value = body.second.evaluate(rootFuncDefs);

        //pop the top mem table
        memStack.remove( memStack.size()-1 );

        // convenience note new top (if any)
        if ( memStack.size() > 0 )
           table = memStack.get( memStack.size() - 1 );
        
        // notice program is over
        else 
        {
           System.out.println(".......done evaluating");
           return value;
        }

      }// user-defined function call

         // uniformly finish
      return value;

    }// funcCall

    else 
    {
      error("Evaluating unknown kind of node [" + kind + "]" );
      return new Value( -1 );
    }

  }// evaluate

   private final static String[] bif0 = { "read", "nl", "quit" };
   private final static String[] bif1 = { "not", "first", "rest", "null", "num", "list", "write", "quote" };
   private final static String[] bif2 = { "plus", "minus", "times", "div", "lt", "le", "eq", "ne", "and", "or", "ins" };

   // return whether target is a member of array
   private static boolean member( String target, String[] array ) {
    for (int k=0; k<array.length; k++) {
     if ( target.equals(array[k]) ) {
      return true;
    }
  }
  return false;
}

   // given a funcCall node, and for convenience its name,
   // locate the function in the function defs and
   // create new memory table with arguments values assigned
   // to parameters
   // Also, return root node of body of the function being called
private static Node passArgs( Node funcCallNode, Node rootFuncDefs, String funcName ) {

      // locate the function in the function definitions

      Node node = rootFuncDefs;  // the funcDefs node
      Node fdnode = null;
      while ( node != null && fdnode == null ) {
         if ( node.first.info.equals(funcName) ) {// found it
          fdnode = node.first;
             //System.out.println("located " + funcName + " at node " + 
               //                  fdnode.id );
        }
        else
         node = node.second;
     }

     MemTable newTable = new MemTable();

      if ( fdnode == null ) {// function not found
       error( "Function definition for [" + funcName + "] not found" );
       return null;
     }
      else {// function name found
         Node pnode = fdnode.first; // current params node
         Node anode = funcCallNode.first;  // current args node
         while ( pnode != null && anode != null ) 
         {
          // store argument value under parameter name
          newTable.store( pnode.first.info,
          anode.first.evaluate(rootFuncDefs) );
          
          // move ahead
          pnode = pnode.second;
          anode = anode.second;
        }

         // detect errors
        if ( pnode != null ) {
          error("there are more parameters than arguments");
        }
        else if ( anode != null ) {
          error("there are more arguments than parameters");
        }

         // manage the memtable stack
        memStack.add( newTable );
        table = newTable;

        return fdnode;  

      }// function name found

   }// passArguments

}// Node
