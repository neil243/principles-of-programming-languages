import java.util.Scanner;
import java.lang.*;
import java.io.*;

public class P11 {

   public static void main(String[] args) throws Exception {

      String name;
      String userInput = null;
      String fileName = "userInput.txt";

      if ( args.length == 1 ) {
         name = args[0];
      }
      else {
         System.out.print("Enter name of P11 program file: ");
         Scanner keys = new Scanner( System.in );
         name = keys.nextLine();
         Lexer lex = new Lexer( name );
         Parser parser = new Parser( lex );
         
         //**For file with function definitions
         // start with <defs>
         Node root = parser.parseDefs();

         System.out.print("\n[Starting REPL...]\n");
         while(true)
         {
            System.out.print("\n%%%>");
            userInput = keys.nextLine();
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
            writer.write(userInput);
            writer.close();
            Lexer lex2 = new Lexer( fileName );
            Parser2 parser2 = new Parser2( lex2 );

            //**For file with user input
            // start with <list>
            Node root2 = parser2.parseList();
            System.out.println( root2.evaluate(root) );

            // display parse tree for debugging/testing:
            //TreeViewer viewer = new TreeViewer("Parse Tree", 0, 0, 800, 500, root );
            //TreeViewer viewer = new TreeViewer("Parse Tree", 0, 0, 800, 500, root2 );
         }

      }

   }// main

}
